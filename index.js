const express = require("express")
const mongoose = require("mongoose")
const bodyParser = require("body-parser")
const CallRecord = require("./CallRecord")

const app = express()
const port = 3000

// Middleware
app.use(bodyParser.json())

MONGO_LOCAL_URL = "mongodb://localhost:27017/call_records"

MONGO_CLOUD =
  "mongodb+srv://shubhamkmr06082001:jl6RXqidtv3Eg1ET@database.mk1kzpb.mongodb.net/call_records"

// MongoDB connection
mongoose.connect(MONGO_CLOUD, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})

// Routes
app.get("/call-records", async (req, res) => {
  try {
    const callRecords = await CallRecord.find()
    res.json(callRecords)
  } catch (error) {
    res.status(500).json({ message: "Internal server error" })
  }
})

app.get("/call-records/:id", async (req, res) => {
  const { id } = req.params
  try {
    const callRecord = await CallRecord.findById(id)
    if (!callRecord) {
      return res.status(404).json({ message: "Call record not found" })
    }
    res.json(callRecord)
  } catch (error) {
    res.status(500).json({ message: "Internal server error" })
  }
})

// NO NEED TO ADD ID IN THE BODY -> as MOngoDb will generate it automatically so skipping it

app.post("/call-records", async (req, res) => {
  const { callerNumber, receiverNumber, startTime, endTime, duration } =
    req.body
  try {
    const newCallRecord = new CallRecord({
      callerNumber,
      receiverNumber,
      startTime: new Date(),
      endTime: new Date(),
      duration,
    })
    const savedCallRecord = await newCallRecord.save()
    res.status(201).json(savedCallRecord)
  } catch (error) {
    res.status(400).json({ message: "Bad request" })
  }
})

app.put("/call-records/:id", async (req, res) => {
  const { id } = req.params
  const { endTime, duration } = req.body
  try {
    const callRecord = await CallRecord.findByIdAndUpdate(
      id,
      { endTime, duration },
      { new: true }
    )
    if (!callRecord) {
      return res.status(404).json({ message: "Call record not found" })
    }
    res.json(callRecord)
  } catch (error) {
    res.status(400).json({ message: "Bad request" })
  }
})

app.delete("/call-records/:id", async (req, res) => {
  const { id } = req.params
  try {
    const deletedRecord = await CallRecord.findByIdAndDelete(id)
    if (!deletedRecord) {
      return res.status(404).json({ message: "Call record not found" })
    }
    res.json({ message: "Call record deleted" })
  } catch (error) {
    res.status(500).json({ message: "Internal server error" })
  }
})

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`)
})
